//
//  OASoundPickerViewController.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OASound;

@protocol OASoundPickerDelegate <NSObject>

- (void)didPickSound:(OASound *)sound;

@end

@interface OASoundPickerViewController : UIViewController

@property (strong, nonatomic) OASound *pickedSound;
@property (weak, nonatomic) id<OASoundPickerDelegate> delegate;

- (void)pickSound:(OASound *)sound;

@end
