//
//  OAResource.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAResource : NSObject

+ (NSArray *)listSound;
+ (void)addSounds:(id)sound;

@end
