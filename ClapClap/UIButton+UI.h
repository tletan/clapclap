//
//  UIButton+UI.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/8/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (UI)

- (void)circle;
- (void)circle:(int)radius;

@end
