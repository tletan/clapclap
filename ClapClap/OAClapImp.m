//
//  OAClapImp.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OAClapImp.h"
#import "OASound.h"
#import "OASetting.h"
#import "OASettingParser.h"

@implementation OAClapImp

- (void)clap:(OASetting *)setting
{
    OASettingParser *parser = [[OASettingParser alloc] initWithSetting:setting];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:[parser soundFilePath] error:nil];
    _player.numberOfLoops = [parser repeat];
    [_player play];
}

@end
