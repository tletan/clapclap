//
//  OAClapViewController.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OAClapImp;
@class OASetting;

@interface OAClapViewController : UIViewController

@property (strong, nonatomic) OASetting *setting;

- (id)initWithClap:(OAClapImp *)clapImp;

@end
