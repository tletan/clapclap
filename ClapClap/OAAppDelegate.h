//
//  OAAppDelegate.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OAClapViewController;

@interface OAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
