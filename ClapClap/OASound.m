//
//  OASound.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OASound.h"

@implementation OASound

- (id)initWithFileName:(NSString *)fileName andDisplayName:(NSString *)displayName
{
    self = [super init];
    if (self) {
        if (!fileName) {fileName = @"yeah-sound";}
        if (!displayName) {displayName = @"yeahhhhh...";}
        _fileName = fileName;
        _displayName = displayName;
    }
    return self;
}

- (id)initWithMediaName:(NSString *)fileName andDisplayName:(NSString *)displayName
{
    self = [self initWithFileName:fileName andDisplayName:displayName];
    self.isMedia = YES;
    return self;
}

- (BOOL)isEqualTo:(OASound *)sound
{
    return [_fileName isEqualToString:sound.fileName] &&
            [_displayName isEqualToString:sound.displayName];
}

- (NSURL *)filePath
{
    if (self.isMedia) {
        return [NSURL URLWithString:self.fileName];
    }
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:self.fileName ofType:@"mp3"];
    return [[NSURL alloc] initFileURLWithPath:soundPath];
}

@end
