//
//  OASetting.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OASound;

@interface OASetting : NSObject

@property (assign, nonatomic) BOOL shuffle;
@property (assign, nonatomic) BOOL repeat;
@property (strong, nonatomic) OASound *sound;

- (void)backup;
+ (OASetting *)restore;

@end
