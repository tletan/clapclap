//
//  OASettingParser.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OASettingParser.h"
#import "OASetting.h"
#import "OASound.h"
#import "OAResource.h"
#import "NSArray+Elements.h"

@interface OASettingParser()

@property (strong, nonatomic) OASetting *setting;

@end
@implementation OASettingParser

- (id)initWithSetting:(OASetting *)setting
{
    self = [super init];
    if (self) {
        _setting = setting;
    }
    return self;
}

- (int)repeat
{
    if (_setting.repeat) {
        return -1;
    }
    return 0;
}

- (NSURL *)soundFilePath
{
    if (_setting.shuffle) {
        OASound *sound = [OAResource listSound].sample;
        return [sound filePath];
    }
    return [_setting.sound filePath];
}

@end
