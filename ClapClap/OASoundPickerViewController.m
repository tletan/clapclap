//
//  OASoundPickerViewController.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OASoundPickerViewController.h"
#import "OASoundPickerTableViewCell.h"
#import "UIView+Nib.h"
#import "OASound.h"
#import <MediaPlayer/MediaPlayer.h>
#import "OAResource.h"
#import "OAAudioPlayer.h"

NSString *const soundPickerIdentifier = @"OASoundPickerCellIdentifier";

@interface OASoundPickerViewController ()<UITableViewDataSource, UITableViewDelegate, MPMediaPickerControllerDelegate>
@property (strong, nonatomic) UITableView *pickerTableView;
@property (strong, nonatomic) NSArray *sounds;

@property (strong, nonatomic) MPMediaPickerController *pickerController;

@end

@implementation OASoundPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _pickerTableView = [[UITableView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_pickerTableView];
    _pickerTableView.delegate = self;
    _pickerTableView.dataSource = self;
    _pickerTableView.tableFooterView = [[UIView alloc] init];
    _pickerTableView.backgroundColor = [UIColor colorWithRed:67/255.0 green:81/255.0 blue:102/255.0 alpha:1];
    [_pickerTableView registerNib:[OASoundPickerTableViewCell defaultNib] forCellReuseIdentifier:soundPickerIdentifier];
    [self mediaPicker];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [OAAudioPlayer stop];
}

- (void)mediaPicker
{
    UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
    bt.frame = CGRectMake(0, 0, 25, 25);
    [bt setBackgroundImage:[UIImage imageNamed:@"audio-picker"] forState:UIControlStateNormal];
    [bt setBackgroundImage:[UIImage imageNamed:@"audio-picker"] forState:UIControlStateHighlighted];
    [bt addTarget:self action:@selector(audioPickerAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBt = [[UIBarButtonItem alloc] initWithCustomView:bt];
    self.navigationItem.rightBarButtonItem = rightBt;
}

- (void)audioPickerAction
{
    self.pickerController = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeAnyAudio];
    if (self.pickerController) {
        self.pickerController.delegate = self;
        self.pickerController.allowsPickingMultipleItems = NO;
        [self presentViewController:self.pickerController animated:YES completion:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _sounds = [OAResource listSound];
    [self.pickerTableView reloadData];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)pickSound:(OASound *)sound
{
    self.pickedSound = sound;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sounds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OASoundPickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:soundPickerIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCell:self.sounds[indexPath.row]];
    cell.checkmark.hidden = ![self.pickedSound isEqualTo:self.sounds[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self pickSound:self.sounds[indexPath.row]];
    [tableView reloadData];
    if ([self.delegate respondsToSelector:@selector(didPickSound:)]) {
        [self.delegate didPickSound:self.pickedSound];
    }
}

- (void) mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection{
    for (MPMediaItem *thisItem in mediaItemCollection.items){
        NSURL     *itemURL = [thisItem valueForProperty:MPMediaItemPropertyAssetURL];
        NSString  *itemTitle = [thisItem valueForProperty:MPMediaItemPropertyTitle];
        NSLog(@"Item URL = %@", itemURL);
        NSLog(@"Item Title = %@", itemTitle);
        OASound *pickedSound = [[OASound alloc] initWithMediaName:[itemURL absoluteString] andDisplayName:itemTitle];
        [OAResource addSounds:pickedSound];
    }
    [mediaPicker dismissViewControllerAnimated:YES completion:nil];
}

@end
