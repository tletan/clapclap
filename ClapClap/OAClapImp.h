//
//  OAClapImp.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@class OASound;
@class OASetting;

@interface OAClapImp : NSObject

@property (strong, nonatomic) AVAudioPlayer *player;

- (void)clap:(OASetting *)setting;

@end
