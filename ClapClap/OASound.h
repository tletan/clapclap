//
//  OASound.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OASound : NSObject

@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic) NSString *displayName;
@property (assign, nonatomic) BOOL isMedia;

- (id)initWithFileName:(NSString *)fileName andDisplayName:(NSString *)displayName;
- (id)initWithMediaName:(NSString *)fileName andDisplayName:(NSString *)displayName;
- (BOOL)isEqualTo:(OASound *)sound;
- (NSURL *)filePath;

@end
