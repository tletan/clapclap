//
//  UIButton+UI.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/8/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "UIButton+UI.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIButton (UI)

- (void)circle
{
    self.layer.cornerRadius = self.frame.size.width/2;
    self.layer.borderWidth = 2;
    self.layer.borderColor = [UIColor orangeColor].CGColor;
}

- (void)circle:(int)radius
{
    self.layer.cornerRadius = radius;
    self.layer.borderWidth = 2;
    self.layer.borderColor = [UIColor orangeColor].CGColor;
}

@end
