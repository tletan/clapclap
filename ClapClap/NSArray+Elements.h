//
//  NSArray+Elements.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Elements)

- (id)sample;

@end
