//
//  OASoundPickerTableViewCell.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OASoundPickerTableViewCell.h"
#import "OASound.h"
#import "OAAudioPlayer.h"

@interface OASoundPickerTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) OASound *sound;

@end

@implementation OASoundPickerTableViewCell

- (void)updateCell:(OASound *)sound
{
    self.name.text = sound.displayName;
    self.sound = sound;
}

- (IBAction)playSound:(id)sender {
    [OAAudioPlayer play:[self.sound filePath]];
}



@end
