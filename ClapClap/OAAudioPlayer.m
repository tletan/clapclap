//
//  OAAudioPlayer.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/8/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OAAudioPlayer.h"
#import <AVFoundation/AVFoundation.h>

@implementation OAAudioPlayer

+ (instancetype)sharedInstance
{
    static OAAudioPlayer *sharedInstance;
    if (!sharedInstance) {
        sharedInstance = [[OAAudioPlayer alloc] init];
    }
    return sharedInstance;
}

- (void)play:(NSURL *)fileName
{
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileName error:nil];
    self.audioPlayer.numberOfLoops = 1;
    [self.audioPlayer play];
}

+ (void)play:(NSURL *)fileName
{
    [[OAAudioPlayer sharedInstance] play:fileName];
}

- (void)stop
{
    [self.audioPlayer stop];
}

+ (void)stop
{
    [[OAAudioPlayer sharedInstance] stop];
}

@end
