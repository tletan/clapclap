//
//  OAAudioPlayer.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/8/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AVAudioPlayer;

@interface OAAudioPlayer : NSObject

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

+ (instancetype)sharedInstance;
+ (void)play:(NSURL *)fileName;
+ (void)stop;

@end
