//
//  OAClapViewController.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OAClapViewController.h"
#import "OAClapImp.h"
#import "OASetting.h"
#import "OASoundPickerViewController.h"
#import "OASound.h"
//#import <

@interface OAClapViewController ()<OASoundPickerDelegate>

@property (strong, nonatomic) OAClapImp *clapImp;
@property (strong, nonatomic) OASoundPickerViewController *pickerController;
@property (weak, nonatomic) IBOutlet UIButton *clapButton;
@property (weak, nonatomic) IBOutlet UIButton *shuffleButton;
@property (weak, nonatomic) IBOutlet UIButton *repeatButton;
@property (weak, nonatomic) IBOutlet UIButton *soundButton;

@end

@implementation OAClapViewController

- (id)initWithClap:(OAClapImp *)clapImp
{
    self = [super init];
    if (self) {
        _clapImp = clapImp;
        _setting = [[OASetting alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor blackColor];
    self.setting = [OASetting restore];
    [self.soundButton setTitle:self.setting.sound.displayName forState:UIControlStateNormal];
    [self.soundButton setTitle:self.setting.sound.displayName forState:UIControlStateHighlighted];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (OASoundPickerViewController *)pickerController
{
    if (!_pickerController) {
        _pickerController = [[OASoundPickerViewController alloc] init];
        _pickerController.delegate = self;
    }
    return _pickerController;
}

- (IBAction)clapAction:(UIButton *)clapButton
{
    [self.clapImp clap:self.setting];
}

- (IBAction)shuffleAction:(UIButton *)shuffleButton
{
    self.setting.shuffle = !_setting.shuffle;
}

- (IBAction)repeatAction:(UIButton *)shuffleButton
{
    self.setting.repeat = !_setting.repeat;
}

- (IBAction)pickAction:(UIButton *)pickButton
{
//    self.pickerController.pickedSound = self.setting.sound;
    [self.navigationController pushViewController:self.pickerController animated:YES];
}

- (void)didPickSound:(OASound *)sound
{
    self.setting.sound = sound;
    [self.soundButton setTitle:sound.displayName forState:UIControlStateNormal];
    [self.soundButton setTitle:sound.displayName forState:UIControlStateHighlighted];
}

@end
