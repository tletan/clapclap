//
//  OAResource.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OAResource.h"
#import "OASound.h"

@interface OAResource()

@property (strong, nonatomic) NSArray *listSound;

@end

@implementation OAResource

+ (instancetype)sharedInstance
{
    static OAResource *sharedInstance;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[OAResource alloc] init];
        [sharedInstance loadSounds];
    });
    return sharedInstance;}

- (void)loadSounds
{
    _listSound = @[];
    NSArray *fileNames = @[ @"yeah-sound", @"clap", @"laughter-and-clap", @"laughter", @"mario-player-down", @"mario-game-over" ];
    NSArray *displayNames = @[ @"yeahhhhh...", @"Clap", @"Laughter and clap", @"Laughter", @"Mario Player Down", @"Mario Game Over" ];
    for (int i = 0; i < fileNames.count; ++i) {
        _listSound = [_listSound arrayByAddingObject:[[OASound alloc] initWithFileName:fileNames[i] andDisplayName:displayNames[i]]];
    }
}

+ (NSArray *)listSound
{
    return [[self sharedInstance] listSound];
}

+ (void)addSounds:(id)sound
{
    [[self sharedInstance] addSound:sound];
}

- (void)addSound:(id)sound
{
    self.listSound = [self.listSound arrayByAddingObject:sound];
}

@end
