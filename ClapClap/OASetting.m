//
//  OASetting.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "OASetting.h"
#import "OASound.h"

NSString *const OASHUFFLE = @"OASHUFFLE";
NSString *const OAREPEAT = @"OAREPEAT";
NSString *const OAFILE_SOUND = @"OAFILE_SOUND";
NSString *const OADISPLAY_NAME = @"OADISPLAY_NAME";
NSString *const OAIS_MEDIA = @"OAIS_MEDIA";

@implementation OASetting

- (void)backup
{
    NSUserDefaults *standUser = [NSUserDefaults standardUserDefaults];
    [standUser setObject:@(self.shuffle) forKey:OASHUFFLE];
    [standUser setObject:@(self.repeat) forKey:OAREPEAT];
    [standUser setObject:self.sound.fileName forKey:OAFILE_SOUND];
    [standUser setObject:self.sound.displayName forKey:OADISPLAY_NAME];
    [standUser setObject:@(self.sound.isMedia) forKey:OAIS_MEDIA];
    [standUser synchronize];
}

+ (OASetting *)restore
{
    NSUserDefaults *standUser = [NSUserDefaults standardUserDefaults];
    OASetting *setting = [[OASetting alloc] init];
    setting.shuffle = [[standUser objectForKey:OASHUFFLE] boolValue];
    setting.repeat = [[standUser objectForKey:OAREPEAT] boolValue];
    if ([[standUser objectForKey:OAIS_MEDIA] boolValue]) {
        setting.sound = [[OASound alloc] initWithMediaName:[standUser objectForKey:OAFILE_SOUND] andDisplayName:[standUser objectForKey:OADISPLAY_NAME]];
    } else {
        setting.sound = [[OASound alloc] initWithFileName:[standUser objectForKey:OAFILE_SOUND] andDisplayName:[standUser objectForKey:OADISPLAY_NAME]];
    }
    return setting;
}

@end
