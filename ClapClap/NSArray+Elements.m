//
//  NSArray+Elements.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import "NSArray+Elements.h"

@implementation NSArray (Elements)

- (id)sample
{
    if ([self count] > 0) {
        return self[arc4random_uniform([self count])];
    }
    return nil;
}

@end
