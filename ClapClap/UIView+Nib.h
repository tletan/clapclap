//
//  UIView+Nib.h
//  KnockToCall
//
//  Created by Phat, Le Tan on 3/7/14.
//  Copyright (c) 2014 OnApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Nib)

+ (UINib *)defaultNib;

@end
