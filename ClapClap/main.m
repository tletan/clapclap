//
//  main.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OAAppDelegate class]));
    }
}
