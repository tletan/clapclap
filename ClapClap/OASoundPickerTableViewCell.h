//
//  OASoundPickerTableViewCell.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <UIKit/UIKit.h>
@class OASound;

@interface OASoundPickerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *checkmark;

- (void)updateCell:(OASound *)sound;

@end
