//
//  OASettingParser.h
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OASetting;

@interface OASettingParser : NSObject

- (id)initWithSetting:(OASetting *)setting;

- (int)repeat;
- (NSURL *)soundFilePath;

@end
