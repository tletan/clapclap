//
//  OASettingParserTests.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/10/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Kiwi.h>
#import "OASettingParser.h"
#import "OASetting.h"
#import "OASound.h"
#import "OAResource.h"
#import "NSArray+Elements.h"

SPEC_BEGIN(OASettingParserSpec)
describe(@"Setting parser", ^{
    __block OASettingParser *sut;
    __block OASetting *mockSetting;
    beforeEach(^{
        mockSetting = [OASetting mock];
        sut = [[OASettingParser alloc] initWithSetting:mockSetting];
    });

    it(@"can parse repeat 0 times to AVAudioPlayer params", ^{
        [mockSetting stub:@selector(repeat) andReturn:theValue(NO)];
        [[@([sut repeat]) should] equal:@0];
    });

    it(@"can parse infinity repeat to AVAudioPlayer params", ^{
        [mockSetting stub:@selector(repeat) andReturn:theValue(YES)];
        [[@([sut repeat]) should] equal:@(-1)];
    });

    it(@"can parse sound file path with no shuffle", ^{
        [mockSetting stub:@selector(shuffle) andReturn:theValue(NO)];
        [mockSetting stub:@selector(sound) andReturn:[[OASound alloc] initWithFileName:@"sound file path" andDisplayName:@""]];
//        [[[sut soundFilePath] should] equal:@"sound file path"];
    });

    it(@"can parse sound file path with shuffle", ^{
        [mockSetting stub:@selector(shuffle) andReturn:theValue(YES)];
        [[OAResource listSound] stub:@selector(sample) andReturn:[OAResource listSound].firstObject];
//        [[[sut soundFilePath] should] equal:@"yeah-sound"];
    });
});
SPEC_END

