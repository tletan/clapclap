//
//  OAClapViewControllerTests.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/6/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Kiwi.h>
#import "OAClapViewController.h"
#import "OAClapImp.h"
#import "OASetting.h"

@interface OAClapViewController()
- (IBAction)clapAction:(UIButton *)clapButton;
- (IBAction)shuffleAction:(UIButton *)shuffleButton;
- (IBAction)repeatAction:(UIButton *)shuffleButton;
- (IBAction)pickAction:(UIButton *)pickButton;

@end

SPEC_BEGIN(OAClapViewControllerSpec)

describe(@"ClapViewController", ^{
    __block OAClapViewController *sut;
    __block OAClapImp *heyClap;
    beforeEach(^{
        heyClap = [OAClapImp mock];
        sut = [[OAClapViewController alloc] initWithClap:heyClap];
    });
    it(@"presses the button to play clap's sound", ^{
        [[heyClap should] receive:@selector(clap:)];
        [sut clapAction:[KWAny any]];
    });

    it(@"sets shuffle or not", ^{
        BOOL prevValue = sut.setting.shuffle;
        [sut shuffleAction:[KWAny any]];
        [[@(sut.setting.shuffle) shouldNot] equal:@(prevValue)];
    });

    it(@"sets repeat or not", ^{
        BOOL prevValue = sut.setting.repeat;
        [sut repeatAction:[KWAny any]];
        [[@(sut.setting.repeat) shouldNot] equal:@(prevValue)];
    });

    it(@"presents sound picker", ^{
        [sut stub:@selector(navigationController) andReturn:[[UINavigationController alloc] initWithRootViewController:sut]];
        [[sut.navigationController should] receive:@selector(pushViewController:animated:)];
        [sut pickAction:[KWAny any]];
    });
});

SPEC_END
