//
//  OASoundTests.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Kiwi.h>
#import "OASound.h"

SPEC_BEGIN(OASoundSpec)

describe(@"Sound model", ^{
    it(@"return YES if has the same file name and display name", ^{
        OASound *sound = [[OASound alloc] initWithFileName:@"" andDisplayName:@""];
        OASound *sound2 = [[OASound alloc] initWithFileName:@"" andDisplayName:@""];
        [[@([sound isEqualTo:sound2]) should] beTrue];
    });

    it(@"return NO if does not have the same file name and display name", ^{
        OASound *sound = [[OASound alloc] initWithFileName:@"DIFF" andDisplayName:@""];
        OASound *sound2 = [[OASound alloc] initWithFileName:@"" andDisplayName:@""];
        [[@([sound isEqualTo:sound2]) should] beFalse];
    });
});

SPEC_END

