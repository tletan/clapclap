//
//  OAClapImpTests.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Kiwi.h>
#import "OAClapImp.h"
#import "OAClapProtocol.h"
#import <AVFoundation/AVFoundation.h>

SPEC_BEGIN(OAClapImpSpecs)

describe(@"Clap client implement", ^{
    __block OAClapImp *sut;
    beforeEach(^{
        sut = [[OAClapImp alloc] init];
    });
    it (@"conforms with clap protocol", ^{
        [sut conformsToProtocol:@protocol(OAClapProtocol)];
    });
    it(@"can play clap sound", ^{
//        AVAudioPlayer *mockPlayer = [AVAudioPlayer mock];
//        [AVAudioPlayer stub:@selector(alloc) andReturn:mockPlayer];
//        [mockPlayer stub:@selector(initWithContentsOfURL:error:) andReturn:mockPlayer];
//        [mockPlayer stub:@selector(setNumberOfLoops:)];
//        [[mockPlayer should] receive:@selector(play)];
//        [sut clap:@""];
    });
});

SPEC_END
