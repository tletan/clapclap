//
//  OASoundPickerViewController.m
//  ClapClap
//
//  Created by Phat, Le Tan on 4/7/14.
//  Copyright (c) 2014 75Lab. All rights reserved.
//

#import <Kiwi.h>
#import "OASoundPickerViewController.h"
#import "OASound.h"

SPEC_BEGIN(OASoundPickerViewControllerSpec)

describe(@"Picker sound", ^{
    it(@"can pick existed sounds", ^{
        OASoundPickerViewController *sut = [[OASoundPickerViewController alloc] init];
        OASound *mockSound = [OASound mock];
        [sut pickSound:mockSound];
        sut.pickedSound = mockSound;
    });
});

SPEC_END